# PhotoOfTheDay

Script for deterministic selecting photo of the day from the library of photos.

## Getting started

python3 select_new_photo.py --glob '/mnt/photos/*' --destination '/var/www/html/photo_of_the_day/'
