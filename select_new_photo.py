import argparse
import pathlib
import glob
from PIL import Image
import subprocess
import shutil
import datetime

file_location = str(pathlib.Path(__file__).parent.resolve())

parser = argparse.ArgumentParser('usage: python select_new_photo.py [options]')
parser.add_argument('--used', action='store', default=file_location + "/used.txt", help='File with already used photos')
parser.add_argument('--glob', default=file_location + "/photos/*", action='store', help='glob search for all photos')
parser.add_argument('--destination',  default=file_location + "/day/", action='store', help='Where to put selected photo')
args = parser.parse_args()

photos_glob = list(glob.glob(args.glob))

if not photos_glob:
    print("No files found")
    exit(-1)

# Load used photos  from file
used = []
try:
    with open(args.used, 'r') as target:
        for line in target.read().splitlines():
            used.append(line)
except FileNotFoundError as e:
    print("No used file found. Creating it.", args.used)


# Filter photos from library -> remove already used photos
photos = list(filter(lambda x: x not in used, photos_glob))

# ALL photos used running second time
if not photos:
    print("ALL photos used running second time")
    shutil.copy2(args.used,args.used + "." + str(int(datetime.datetime.now().timestamp())) + ".backup")
    used = []
    photos = photos_glob

# Sort photos by hash so it has determined order
photos = [(photo, int(hash(photo.split('/')[-1]))) for photo in photos]
photos.sort(key=lambda tup: tup[1])

# Select first photo
selected_photo_index = 0
# Iterate through files until the
while photos:
    used.append(photos[selected_photo_index][0])
    try:
        im=Image.open(photos[selected_photo_index][0])
        break
    except IOError:
        print("Image", photos[selected_photo_index][0], "is not valid photo or is corrupted.")
        selected_photo_index += 1
        del photos[selected_photo_index]

selected_photo = photos[selected_photo_index][0]
with open(args.used, 'w') as target:
    target.write('\n'.join(used))

# convert file to jpg and place it to specified location
new_photo_file = args.destination.rstrip("/") + "/actual.jpg"
bashCommand = "convert -limit memory 50M -limit map 50M " + selected_photo + " " + new_photo_file
retCode = subprocess.call(bashCommand, shell=True)
if retCode != 0:
    print("Imagemagic convert failed. $ >", bashCommand)


# Create new monochrome image
new_monochrome_file = args.destination.rstrip("/") + "/monochrome_800x480.bmp"
bashCommand = "convert " + new_photo_file + " -limit memory 50M -limit map 50M -resize 800x480! -dither FloydSteinberg -define dither:diffusion-amount=100% -remap pattern:gray50 " + new_monochrome_file
retCode = subprocess.call(bashCommand, shell=True)
if retCode != 0:
    print("Imagemagic convert failed. $ >", bashCommand)
else:
    # binarize monochrome
    binarized = []
    im = Image.open(new_monochrome_file)
    width, height = im.size
    for y in range(height):
        for x in range(int(width/8)):
            value = 0
            for i in range(8):
                v = 1 if im.getpixel((x*8+i,y)) > 128 else 0
                value += pow(2, 7-i) * v
            binarized.append(int(value))


    binarized_file = new_monochrome_file + ".bin"
    with open(binarized_file, 'wb') as target:
        binarized = bytes(binarized)

        target.write(binarized)
